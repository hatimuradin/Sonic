#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <gst/gst.h>
#include <iostream>
#include <thread>
#include "SequencerAudioPlayerMsg.h"

#ifndef AUDIO_PLAYER_H
#define AUDIO_PLAYER_H

namespace Sonic{
    namespace CapabilityAgents{
        
        using namespace Sonic::Common::Messages;

        class AudioPlayer{
        public:
        /* Structure to contain all our information, so we can pass it around */
            typedef struct _CustomData {
                GstElement *playbin;  /* Our one and only element */

                gint n_video;          /* Number of embedded video streams */
                gint n_audio;          /* Number of embedded audio streams */
                gint n_text;           /* Number of embedded subtitle streams */

                gint current_video;    /* Currently playing video stream */
                gint current_audio;    /* Currently playing audio stream */
                gint current_text;     /* Currently playing subtitle stream */

                GMainLoop *main_loop;  /* GLib's Main Loop */
                int EOS;
            } CustomData;

            /* playbin flags */
            typedef enum {
                GST_PLAY_FLAG_VIDEO         = (1 << 0), /* We want video output */
                GST_PLAY_FLAG_AUDIO         = (1 << 1), /* We want audio output */
                GST_PLAY_FLAG_TEXT          = (1 << 2)  /* We want subtitle output */
            } GstPlayFlags;
            
            CustomData data;
            GstBus *bus;
            GstStateChangeReturn ret;
            gint flags;
            GIOChannel *io_stdin;
            std::string uri;
            AudioPlayerMessage apmsg;
            SequencerAudioPlayerMsg* seqapmsg;
            /* Forward definition for the message and keyboard processing functions */
            static void analyzeStreams (CustomData*);
            static gboolean handleMessage (GstBus*, GstMessage*, CustomData*);
            static gboolean handleKeyboard (GIOChannel*, GIOCondition, CustomData*);
            void doPlay();
            AudioPlayer(SequencerAudioPlayerMsg&);
            ~AudioPlayer();
        private:
        };
    }// Capability Agents
}//Sonic

#endif  /* AUDIO_PLAYER_H */