/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SpeechRecognizer.h
 * Author: Hatef
 *
 * Created on June 23, 2018, 9:35 AM
 */
#include "KWD.h"
#include "Recorder.h"
#include "RecorderCommunicationMsg.h"

#ifndef SPEECHRECOGNIZER_H
#define SPEECHRECOGNIZER_H

using namespace Sonic::AIP;
using namespace Sonic::Common::Messages;

namespace Sonic{
    namespace CapabilityAgents{
        
        class SpeechRecognizer{          
        public:
            std::vector<short> voice_buffer;
            RecorderCommunicationMsg* rcmsg;
            HardwareMsg* hwmsg;
            
            void start();
            SpeechRecognizer(RecorderCommunicationMsg&);
            ~SpeechRecognizer();
        private:
            KWD* kwd;
            Recorder* recorder;
            
        };
        
    }
}

#endif /* SPEECHRECOGNIZER_H */

