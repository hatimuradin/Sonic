/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "SpeechRecognizer.h"

namespace Sonic
{
    namespace CapabilityAgents{

        void SpeechRecognizer::start()
        {
            //while(true){
            //Detect the Keyword
            kwd->phraseSpot();
            std::cout << "after kwd\n";
            //Record
            
            recorder->recogEnroll();
            std::cout << "after enroll\n";
            for (auto i=0; i<voice_buffer.size(); i++){
                //std::cout << voice_buffer[i] << "\t";
                rcmsg->audio.push_back(voice_buffer[i]);
            }
            rcmsg->audio_length = (unsigned int)voice_buffer.size();
            voice_buffer.clear();

            std::cout << "after move\n";
            //rcmsg->audio.push_back(std::move(voice_buffer));
            //Check Directive
            //}
        }
        SpeechRecognizer::SpeechRecognizer(RecorderCommunicationMsg& rcmsg_)
        {
            rcmsg = &rcmsg_;
            std::cout << "before creating kwd moduel\n";
            hwmsg = new HardwareMsg();
            kwd = new KWD(*hwmsg);
            std::cout << "after creating recorder module\n";
            recorder = new Recorder(voice_buffer, *hwmsg);
            std::cout << "after creating recorder module\n";

        }
        SpeechRecognizer::~SpeechRecognizer()
        {
            delete kwd;
            delete recorder;
        }
    }//Capability Agents namespace
}//Sonic namespace