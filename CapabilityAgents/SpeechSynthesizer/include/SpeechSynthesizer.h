#include <alsa/asoundlib.h>
#include <iostream>
#include <unistd.h>
#include <thread>
#include "SequencerSpeechSynthesizerMsg.h"

#ifndef SPEECH_SYNTHESIZER_H
#define SPEECH_SYNTHESIZER_H

namespace Sonic{
    namespace CapabilityAgents{
        
        using namespace Sonic::Common::Messages;

        class SpeechSynthesizer{
        public:
        	unsigned int pcm, tmp, dir, rate, channels, seconds;
	        snd_pcm_t *pcm_handle;
	        snd_pcm_hw_params_t *params;
	        snd_pcm_uframes_t frames;
	        char *buff;
	        int buff_size, loops, cc, rc;
            SpeechSynthesizerMessage ssmsg;
            SequencerSpeechSynthesizerMsg* seqssmsg;
            void doPlay();
            SpeechSynthesizer(SequencerSpeechSynthesizerMsg&);
            ~SpeechSynthesizer();
        private:
        };
    }// Capability Agents
}//Sonic

#endif  /* SPEECH_SYNTHESIZER_H */
