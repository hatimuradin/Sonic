#include "SpeechSynthesizer.h"
#include "Logger.h"

namespace Sonic{
    namespace CapabilityAgents{

#define LX(event) Sonic::Common::logger::LogEntry(TAG, event)
        static const std::string TAG("SpeechSynthesizer");

		void SpeechSynthesizer::doPlay(){
			while(true){
				if (!seqssmsg->messages_queue.empty()){
					ssmsg = seqssmsg->messages_queue.front();
					std::cout << "AP Audio Size: " << ssmsg.audio.size() << "\n";
					printf("AP LAST CHUNK: %x\n", ssmsg.audio[ssmsg.audio.size()-1]);

					seqssmsg->messages_queue.pop();

					cc = ssmsg.audio.size()/buff_size;
					rc = ssmsg.audio.size()%buff_size;
					std::cout << "correct component: " << cc << "\n";
					std::cout << "remincer component: " << rc << "\n";

            		//printf("first  100 bytes of audio before send to server in transfer module\n");
            		//for (int i=0 ; i<100; i++) printf("%02x \n", tmp_audio_char[i]/*rcmsg->audio.data()[i]*/);

					for (loops=0; loops<cc; loops++){
						//std::cout << "Sample buffer index: " << ssmsg.audio[loops*buff_size+8] << "\n";

						memcpy(buff, &(ssmsg.audio.data()[loops*buff_size]), buff_size);
						/*
						if (loops==1){
							for (int i=0; i<buff_size; i++){
								printf("%x ", buff[i]);
							}
							printf("---------------------\n");
						}*/

						if(pcm = snd_pcm_writei(pcm_handle, buff, frames) == -EPIPE){
							ACSDK_ERROR(LX("XRUN."));
							snd_pcm_prepare(pcm_handle);
						} else if (pcm < 0){
							ACSDK_ERROR(LX("Can't write to PCM device.").d("snd error is: ", snd_strerror(pcm)));
						}
					}
					//std::cout << "loops" << loops << "\n";

					memcpy(buff, &(ssmsg.audio.data()[loops*buff_size]), rc);
					if(pcm = snd_pcm_writei(pcm_handle, buff, frames) == -EPIPE){
						ACSDK_ERROR(LX("XRUN."));
						snd_pcm_prepare(pcm_handle);
					} else if (pcm < 0){
						ACSDK_ERROR(LX("Can't write to PCM device.").d("snd error is: ", snd_strerror(pcm)));
					}

					//std::cout << "injam ... \n";
				}
				usleep(100000);
			}//infinite loop
		}//doPlay

        SpeechSynthesizer::SpeechSynthesizer(SequencerSpeechSynthesizerMsg& seqssmsg_){
			
			seqssmsg = &seqssmsg_;
            rate = 16000;
            channels = 1;
            /* Open the PCM device in playback mode */
	        if (pcm = snd_pcm_open(&pcm_handle, "default",
					SND_PCM_STREAM_PLAYBACK, 0) < 0) 
		        ACSDK_ERROR(LX("Can't open PCM device.").d("snd error is: ", snd_strerror(pcm)));
            
            /* Allocate parameters object and fill it with default values*/
	        snd_pcm_hw_params_alloca(&params);

	        snd_pcm_hw_params_any(pcm_handle, params);
            /* Set parameters */
	        if (pcm = snd_pcm_hw_params_set_access(pcm_handle, params,
				SND_PCM_ACCESS_RW_INTERLEAVED) < 0) 
		        ACSDK_ERROR(LX("Can't set interleaved mode.").d("snd error is: ", snd_strerror(pcm)));

	        if (pcm = snd_pcm_hw_params_set_format(pcm_handle, params,
				SND_PCM_FORMAT_S16_LE) < 0) 
		        ACSDK_ERROR(LX("Can't set format.").d("snd error is: ", snd_strerror(pcm)));

	        if (pcm = snd_pcm_hw_params_set_channels(pcm_handle, params, channels) < 0) 
		        ACSDK_ERROR(LX("Can't set channels number.").d("snd error is: ", snd_strerror(pcm)));

	        if (pcm = snd_pcm_hw_params_set_rate_near(pcm_handle, params, &rate, 0) < 0) 
		        ACSDK_ERROR(LX("Can't set rate.").d("snd error is: ", snd_strerror(pcm)));

	        /* Write parameters */
	        if (pcm = snd_pcm_hw_params(pcm_handle, params) < 0)
		        ACSDK_ERROR(LX("Can't set harware parameters.").d("snd error is: ", snd_strerror(pcm)));

	        /* Resume information */
	        ACSDK_INFO(LX("PCM name:").d("snd info is: ", snd_pcm_name(pcm_handle)));

	        ACSDK_INFO(LX("PCM state:").d("snd info is: ", snd_pcm_state_name(snd_pcm_state(pcm_handle))));

	        snd_pcm_hw_params_get_channels(params, &tmp);
	        ACSDK_INFO(LX("Number of ").d("Channels: ", tmp));

	        if (tmp == 1)
		    	ACSDK_INFO(LX("(mono)"));
	        else if (tmp == 2)
		    	ACSDK_INFO(LX("(stereo)"));

	        snd_pcm_hw_params_get_rate(params, &tmp, 0);
	        ACSDK_INFO(LX("rate: ").d("bps", tmp));

	        //ACSDK_INFO(LX("Length").d("seconds", seconds));

            /* Allocate buffer to hold single period */
	        snd_pcm_hw_params_get_period_size(params, &frames, 0);

			std::cout << "frames: " << frames << "\n";
			std::cout << "channel: " << channels << "\n";
	        buff_size = frames * channels * 2;
			std::cout << "Buff Size: " << buff_size << "\n";
	        buff = (char *)malloc(buff_size);

            std::thread t(&SpeechSynthesizer::doPlay, this);
            t.join();
        }//SpeechSynthesizer()

    }//Capability Agents
}//Sonic