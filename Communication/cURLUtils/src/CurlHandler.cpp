#include "Logger.h"
#include "CurlHandler.h"

namespace Sonic {
    namespace Communication {
        /**
         * Create a LogEntry using this file's TAG and the specified event string.
         *
         * @param The event string for this @c LogEntry.
         */
#define LX(event) Sonic::Common::logger::LogEntry(TAG, event)
        static const std::string TAG("CURL");

        size_t CurlHandler::headerCallback(char *contents, size_t size, size_t nitems, void *userdata){
            size_t realsize = nitems * size;

            struct HTTPHeader * header = (struct HTTPHeader *)userdata;
            header->memory = (char*)realloc(header->memory, header->size + realsize);
            if (header->memory == NULL) {
                /* out of memory! */
                return 0;
            }
            
            memcpy(&(header->memory[header->size]), contents, realsize);
            header->size += realsize;
            //header->memory[header->size] = 0;

            return nitems * size;
        }


        size_t CurlHandler::bodyCallback(void *contents, size_t size, size_t nmemb, void *userp) {
            size_t realsize = size * nmemb;
            struct HTTPBody *body = (struct HTTPBody *) userp;
            //std::cout << "size of body callback received data: " << realsize << "\n";
            body->memory = (char*)realloc(body->memory, body->size + realsize);
            if (body->memory == NULL) {
                /* out of memory! */
                return 0;
            }

            //for (int i=0; i<realsize; i++) printf("%c", *(char*)contents[i]);

            memcpy(&(body->memory[body->size]), contents, realsize);
            body->size += realsize;
            //body->memory[body->size] = 0;

            return realsize;
        }

        int CurlHandler::communicate(const char* json, unsigned int json_length, char* audio, unsigned int audio_length,
         std::string* received_header, std::string* received_body) {

            curl_formadd(&formpost,
                    &lastptr,
                    CURLFORM_COPYNAME, "metadata",
                    CURLFORM_COPYCONTENTS, json,
                    CURLFORM_CONTENTTYPE, "application/json",
                    //CURLFORM_CONTENTHEADER, headers,
                    //CURLFORM_BUFFER, "testfile",
                    //CURLFORM_BUFFERPTR, json,
                    //CURLFORM_BUFFERLENGTH, json_length,
                    CURLFORM_END);

            char* test = new char[256];
            for (int i=0; i<256; i++){
                test[i]= (char) i;
            }
            
            curl_formadd(&formpost,
                    &lastptr,
                    CURLFORM_COPYNAME, "audio",
                    //CURLFORM_STREAM, audio,
                    CURLFORM_CONTENTTYPE, "application/octet-stream",
                    //CURLFORM_BUFFER, "testfile",
                    CURLFORM_BUFFERPTR, audio,
                    CURLFORM_BUFFERLENGTH, audio_length,
                    CURLFORM_END);
            //init easy_curl

            curl = curl_easy_init();

            if (!curl)
                return 1;

            curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_PORT, port);
            curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
            curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
            curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L); 
	        curl_easy_setopt(curl, CURLOPT_TCP_KEEPIDLE, 3600L);
    	    curl_easy_setopt(curl, CURLOPT_TCP_KEEPINTVL, 60L);
            curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
            curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
            curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
            curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, headerCallback);
            curl_easy_setopt(curl, CURLOPT_HEADERDATA, (void *) &header);
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, bodyCallback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *) &body);
            //curl_easy_setopt(curl, CURLOPT_DEFAULT_PROTOCOL, "https");
            //curl_easy_setopt(curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            //curl_easy_setopt(curl, CURLOPT_USE_SSL, CURLUSESSL_ALL);
            //curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
            //curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);

            int res = curl_easy_perform(curl);
            
            std::string tmp_header_str(header.memory, header.size);
            std::string tmp_body_str(body.memory, body.size);

            *received_header = tmp_header_str;
            *received_body = tmp_body_str;

            std::cout << "header size in curl: " << received_header->length() << "\n";
            std::cout << "body size in curl: " << received_body->length() << "\n";

            //for (int i=0; i<body.size; i++) printf("%c", body.memory[i]);
            //std::cout << "Received HTTP header in CURL Begins Here: \n" << header.memory << "Ends Here\n";
            //std::cout << "Received HTTP body in CURL Begins Here \n" << body.memory << "Ends Here\n";

            //ACSDK_ERROR(LX("curl easy perform failed").d("reason", "idk"));
        }

        CurlHandler::CurlHandler(std::string url_, int port_) {
            /*-----CURL SETUP-----*/
            //Set headers as null
            url = url_;
            port = port_;
            headers = NULL;
            formpost = NULL;
            lastptr = NULL;
            header.memory = (char*)malloc(1);  /* will be grown as needed by the realloc above */ 
            header.size = 0;    /* no data at this point */ 
            body.memory = (char*)malloc(1);
            body.size = 0;

            curl_global_init(CURL_GLOBAL_ALL);

            headers = curl_slist_append(headers, "Content-Type: multipart/form-data");

        }

        CurlHandler::~CurlHandler() {
            free(header.memory);
            free(body.memory);
            curl_easy_cleanup(curl);
            curl_formfree(formpost);
            curl_slist_free_all(headers);
        }
    }
}