/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Transfer.cpp
 * Author: Hatef
 * 
 * Created on June 24, 2018, 10:57 AM
 */

#include "Logger.h"
#include "Transfer.h"
#include <fstream>
#include <iostream>

namespace Sonic {
    namespace Communication {

#define LX(event) Sonic::Common::logger::LogEntry(TAG, event)
        static const std::string TAG("Transfer");

        void Transfer::doTransmit(){
        printf("Reaches Here\n");

            rapidjson::StringBuffer sb;
            rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(sb);
            
            rcmsg->Serialize(writer);
            
            char* tmp_audio_char = (char*)malloc(rcmsg->audio_length*2*sizeof(char));
            memcpy(tmp_audio_char, rcmsg->audio.data(), rcmsg->audio_length*2);
            //printf("first  100 bytes of audio before send to server in transfer module\n");
            //for (int i=0 ; i<100; i++) printf("%02x \n", tmp_audio_char[i]/*rcmsg->audio.data()[i]*/);

            curl_handler->communicate((const char*)sb.GetString(), 
                                                    (unsigned int) sb.GetLength(), 
                                                    tmp_audio_char, 
                                                    rcmsg->audio_length*2,
                                                    &received_header,
                                                    &received_body
                                                    );
            
            //std::cout << "Received HTTP header Begins Here: \n" << received_header << "Ends Here\n";
            //std::cout << "Received HTTP body Begins Here: \n" << received_body << "Ends Here\n";
            
            re_boundary = "[^]*boundary=(.+)\r\n[^]*";

	        std::regex_match(received_header, match_results, re_boundary);
            if (match_results.size() < 2) //One for full match sequence and Another for catched group
            {
                ACSDK_ERROR(LX("Cannot find header boundary").d("Reason", "Regex Match[1] was empty."));
            }

            std::string boundary = match_results[1].str();

            std::cout << "boundary is: " << boundary << "\n";
            
            /*
            re_json = "[^]*application/json\r\n\r\n(.+)\r\n--" + boundary + "[^]*";
            re_audio = "[^]*application/octet-stream\r\n\r\n([^]*)\r\n--" + boundary + "--[^]*";


            std::regex_match(received_body, match_results, re_json);
            if (match_results.size() < 2) //One for full match sequence and Another for catched group
            {
                ACSDK_ERROR(LX("Cannot find body's json part").d("Reason", "Regex Match[1] was empty."));
            }
            std::string received_json = match_results[1].str();

            std::regex_match(received_body, match_results, re_audio);
            if (match_results.size() < 2) //One for full match sequence and Another for catched group
            {
                ACSDK_ERROR(LX("Cannot find body's audio part").d("Reason", "Regex Match[1] was empty."));
            }
            std::string received_audio = match_results[1].str();
            */
            std::string before_json_delimiter = "application/json\r\n\r\n";
            unsigned before_json = received_body.find(before_json_delimiter);
            unsigned after_json = received_body.find("\r\n--" + boundary, 1);

            std::string before_audio_delimiter = "application/octet-stream\r\n\r\n";
            unsigned before_audio = received_body.find(before_audio_delimiter);
            unsigned after_audio = received_body.find("\r\n--" + boundary + "--");
            
            //std::cout << "before_json: " << before_json << "\n";
            //std::cout << "after_json: " << after_json << "\n";
            //std::cout << "before_audio: " << before_audio << "\n";
            //std::cout << "after_audio: " << after_audio << "\n";

            received_json = received_body.substr(before_json + before_json_delimiter.length(), after_json-(before_json+before_json_delimiter.length()));
            received_audio = received_body.substr(before_audio + before_audio_delimiter.length(), after_audio-(before_audio+before_audio_delimiter.length()));

            //std::cout << "Received Audio Size: " << received_audio.length() << "\n";
            //std::cout << "Received Json: " << received_json << "\n";
            //std::cout << "Received Audio: " << received_audio << "\n";
            //std::cout << "Received Body: " << received_body << "\n";

            std::cout << "Received Json: " << received_json << "\n";
            std::ofstream out("test_audio.wav");
            out << received_audio;
            out.close();

            std::vector<char> tmp_audio_char_vector(received_audio.begin(), received_audio.end());

            sequencer_qmessage.json = received_json;
            /*
            for (unsigned i=0; i<received_audio.length(); i++){
                sequencer_qmessage.audio.push_back((short)received_audio[i]);
            }
            */
            //short* tmp_audio_short_vector = reinterpret_cast<short*>(tmp_audio_char_vector.data());
            //int alength = sizeof(tmp_audio_short_vector)/sizeof(tmp_audio_short_vector);
            //for (int i=0; i<alength; i++)   sequencer_qmessage.audio.push_back(tmp_audio_short_vector[i]);
            sequencer_qmessage.audio = tmp_audio_char_vector;
            csmsg->messages_queue.push(sequencer_qmessage);
            std::cout << "json msg inside Transfer: " << sequencer_qmessage.json << "\n";

        }
        
        Transfer::Transfer(RecorderCommunicationMsg& rcmsg_, CommunicationSequencerMsg& csmsg_, std::string url_, int port_) {
            printf("Reaches Here\n");
            rcmsg = &rcmsg_;
            csmsg = &csmsg_;
            curl_handler = new CurlHandler(url_, port_);
            std::thread t(&Transfer::doTransmit, this);
            t.join();
        }

        Transfer::~Transfer() {
        }
        
    }//namespace Communication
}//namespace Sonic
