#include <cstdlib>
#include <iostream>
#include "Transfer.h"
#include "SpeechRecognizer.h"

using namespace Sonic::Common::Messages;
using namespace Sonic::Communication;
using namespace Sonic::CapabilityAgents;
        
int main(int argc ,char* argv[]){
    
    RecorderCommunicationMsg* rcmsg = new RecorderCommunicationMsg();
    CommunicationSequencerMsg* csmsg = new CommunicationSequencerMsg();

    SpeechRecognizer* sr = new SpeechRecognizer(*rcmsg);
    sr->start();

    Transfer* tr = new Transfer(*rcmsg, *csmsg, std::string(argv[1]), atoi(argv[2]));

    //std::cout << "Filled Message Json: " << csmsg->messages_queue.front().json << "\n";
    //std::cout << "Filled Message Audio: " << csmsg->Messages.front().audio << "\n";
    
    return 0;
}

