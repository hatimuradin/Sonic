/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CurlHandler.h
 * Author: Hatef
 *
 * Created on June 25, 2018, 11:40 AM
 */
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <iostream>

#ifndef CURLHANDLER_H
#define CURLHANDLER_H

namespace Sonic{
    namespace Communication{
 
        struct HTTPHeader{
	        char *memory;
	        size_t size;
        };
               
        struct HTTPBody{
	        char *memory;
	        size_t size;
        };

        class CurlHandler{
        public:
            
            std::string url;
            long port;
            struct curl_slist *headers;
            struct curl_httppost *formpost;
            struct curl_httppost *lastptr;
            CURL* curl;
            struct HTTPHeader header;
            struct HTTPBody body;
            static size_t headerCallback(char *, size_t, size_t, void *);
            static size_t bodyCallback(void *, size_t, size_t, void *);
            int communicate(const char*, unsigned int, char*, unsigned int, std::string*, std::string*);
            CurlHandler(std::string, int);
            ~CurlHandler();
            
        private:
        };
    }
}

#endif /* CURLHANDLER_H */

