/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Transfer.h
 * Author: Hatef
 *
 * Created on June 24, 2018, 10:57 AM
 */
#include <thread>
#include <string>
#include <regex>
#include "RecorderCommunicationMsg.h"
#include "CommunicationSequencerMsg.h"
#include "rapidjson/prettywriter.h"
#include "CurlHandler.h"

using namespace Sonic::Common::Messages;

#ifndef TRANSFER_H
#define TRANSFER_H

namespace Sonic {
    namespace Communication {

        class Transfer {
        public:
            std::regex re_boundary, re_json, re_audio;
            std::smatch match_results;
            std::string received_header, received_body, received_json, received_audio;
            short short_received_audio;
            RecorderCommunicationMsg* rcmsg;
            Sonic::Common::Messages::SequencerMessage sequencer_qmessage;
            CommunicationSequencerMsg* csmsg;
            CurlHandler* curl_handler;
            void doTransmit();
            Transfer(RecorderCommunicationMsg& rcmsg_, CommunicationSequencerMsg& csmsg_, std::string, int);
            ~Transfer();
        private:
        };
    }
}
#endif /* TRANSFER_H */

