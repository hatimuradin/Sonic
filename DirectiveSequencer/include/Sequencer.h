/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SEQUENCER.h
 * Author: Hatef
 *
 * Created on July 3, 2018, 13:00 PM
 */

#ifndef SEQUENCER_H
#define SEQUENCER_H

#include <iostream>
#include <string>
#include <thread>
#include "rapidjson/document.h"
#include "CommunicationSequencerMsg.h"
#include "SequencerSpeechSynthesizerMsg.h"

namespace Sonic{
    using namespace Sonic::Common::Messages;
    class Sequencer{
        public:
            enum directive_code {eSpeechRecognizer, eSpeechSynthesizer, eAudioPlayer};
            directive_code directive;
            SequencerMessage poped_msg;
            SpeechSynthesizerMessage ssmsg;
            rapidjson::Document doc;
            CommunicationSequencerMsg* csmsg;
            SequencerSpeechSynthesizerMsg* seqssmsg;
            void doDispatch();
            Sequencer(CommunicationSequencerMsg&, SequencerSpeechSynthesizerMsg&);
            ~Sequencer();
        private:
    };
}

#endif /* SEQUENCER_H */