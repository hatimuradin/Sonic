#include <iostream>
#include "Sequencer.h"

using namespace Sonic::Common::Messages;
using namespace Sonic;

int main(){

    CommunicationSequencerMsg* csmsg = new CommunicationSequencerMsg();
    SequencerAudioPlayerMsg* seqapmsg = new SequencerAudioPlayerMsg();

    SequencerMessage seqmsg;
    seqmsg.json = "{\"Directive\":\"AudioPlayer\"}";
    for (int i=0; i<20; i++){
        seqmsg.audio.push_back((short)i);
    }
    csmsg->messages_queue.push(seqmsg);
    Sequencer* mysq = new Sequencer(*csmsg, *seqapmsg);
    AudioPlayerMessage apmsg = seqapmsg->messages_queue.front();
    std::cout << "Received AudioPlayer Message Contain This var1: " << apmsg.var1 << "\n";
    return 0;
}