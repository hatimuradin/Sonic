cmake_minimum_required(VERSION 3.1 FATAL_ERROR)
project(Sequencer)

SET(SOURCE_PATH ${CMAKE_SOURCE_DIR}/../src)
SET(SONIC_PATH /home/pi/Sonic)


include_directories(${SONIC_PATH}/Common)
include_directories(${SONIC_PATH}/Common/Messages)
include_directories(${SONIC_PATH}/Common/Logger/include)
include_directories(${CMAKE_SOURCE_DIR}/../include)

link_directories(${SONIC_PATH}/Common/Logger)

add_executable(Sequencer-test Sequencertest.cpp ${SOURCE_PATH}/Sequencer.cpp)

target_link_libraries(Sequencer-test -lpthread -llogger)
