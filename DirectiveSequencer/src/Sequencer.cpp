/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Transfer.cpp
 * Author: Hatef
 * 
 * Created on July 3 , 2018, 13:55 PM
 */

#include "Sequencer.h"

namespace Sonic{

    void Sequencer::doDispatch(){
        
        poped_msg = csmsg->messages_queue.front();
        csmsg->messages_queue.pop();
        std::cout << poped_msg.json << "\n";
        doc.Parse(poped_msg.json.c_str());
        std::cout << "reached here\n";
        std::cout << doc["Directive"].GetString();
        if (doc["Directive"] == "SpeechRecognizer") directive = eSpeechRecognizer;
        if (doc["Directive"] == "SpeechSynthesizer") directive = eSpeechSynthesizer;
        if (doc["Directive"] == "AudioPlayer") directive = eAudioPlayer;

        switch(directive){
            case eSpeechRecognizer: {
                std::cout << "SpeechRecognizer Received.\n";
                break;
            }
            case eSpeechSynthesizer: {
                std::cout << "SpeechSynthesizer Received.\n";
                ssmsg.option1=5;
                ssmsg.option2=19;
                ssmsg.audio=poped_msg.audio;
                seqssmsg->messages_queue.push(ssmsg);
                break;
            }
            case eAudioPlayer: {
                //AudioPlayerMessage apmsg;
                //apmsg.var1=7;
                //apmsg.var2=11;
                //apmsg.audio = poped_msg.audio;
                //seqapmsg->messages_queue.push(apmsg);

                std::cout << "AudioPlayer Received.\n";
                break;
            }
            default: {
                std::cout << "Directive not recognized.\n";
            }
        }
    }

    Sequencer::Sequencer(CommunicationSequencerMsg& csmsg_, SequencerSpeechSynthesizerMsg& seqssmsg_){
        csmsg = &csmsg_;
        seqssmsg = &seqssmsg_;
        std::thread t(&Sequencer::doDispatch, this);
        t.join();
    }
    
    Sequencer::~Sequencer(){

    }

  
}