#include "Recorder.h"
#include "stdio.h"
/*------------------------------------------------------------------------------*/
/* Define a small value so that printf() yields the same output with
   different compilers; MS always rounds up, gcc (?) uses "banker's rounding" */

namespace Sonic {
    namespace AIP {

        void Recorder::printValue(void *cons, void * job, void * inst,
                int rIdx, const char *valName,
                thfEnrollmentCheckOne_t *checkZero, thfEnrollmentCheckOne_t *checkThis) {
            char nomark[] = "  ";
            char mark[] = "**";
            char str[MAXSTR];
            sprintf(str, "%-11s %s %-8.2f (%.2f)", valName,
                    checkThis->pass[rIdx] ? nomark : mark,
                    checkThis->score[rIdx] + EPSILON,
                    checkZero->thresh[rIdx] + EPSILON);
            disp(cons, str);
        }

        void Recorder::printValues(void * cons, void * job, void * inst,
                thfEnrollmentCheck_t *enrollmentCheckResult, int eIdx) {
            char str[MAXSTR];
            thfEnrollmentCheckOne_t *checkZero = &(enrollmentCheckResult->enroll[0]);
            thfEnrollmentCheckOne_t *checkThis = &(enrollmentCheckResult->enroll[eIdx]);

            sprintf(str, "Check File %d: ", eIdx);
            disp(cons, str);
            sprintf(str, "%-13s  %-8s %-11s", "Attribute", "Value", "(Threshold)");
            disp(cons, str);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_ENERGY_MIN, "ENERGY_MIN", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_ENERGY_STD_DEV, "ENERGY_VAR", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_SIL_BEG_MSEC, "SIL_BEG(ms)", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_SIL_END_MSEC, "SIL_END(ms)", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_SNR, "SNR", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_RECORDING_VARIANCE, "REC VAR", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_POOR_RECORDINGS_LIMIT, "POOR REC", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_CLIPPING_FRAMES, "CLIP (fr)", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_VOWEL_DUR, "VOWEL_DUR", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_REPETITION, "REPETITION", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_SIL_IN_PHRASE, "SILENCE", checkZero, checkThis);
            printValue(cons, NULL, NULL, CHECK_TRIGGER_SPOT, "SPOT", checkZero, checkThis);
        }

        void Recorder::printEnrollFeedback(void *cons, void * job, void *inst,
                thfEnrollmentCheck_t *enrollmentCheckResult) {
            int eIdx;
            disp(cons, "");
            for (eIdx = 0; eIdx < enrollmentCheckResult->numEnroll; eIdx++) {
                printValues(cons, NULL, NULL, enrollmentCheckResult, eIdx);
                disp(cons, "");
            }
            disp(cons, "");
            return;
        }

        /* ------------------------------------------------------------------------------ */
        void Recorder::printTaskInfo(void *cons, void *job, void* inst, thf_t *ses, adaptObj_t *aObj, char *taskName) {
            thfAdaptTaskInfo_t *taskInfo = NULL;
            char str[MAXSTR];
            thfAdaptTaskInfo(ses, aObj, taskName, &taskInfo);
            if (taskInfo) {
                disp(cons, "Task Parameter Settings:");
                sprintf(str, "  UDT            \t: %d", taskInfo->userDefinedTrigger);
                disp(cons, str);
                sprintf(str, "  featureType    \t: %d", taskInfo->featureType);
                disp(cons, str);
                sprintf(str, "  sampleRate     \t: %d", taskInfo->sampleRate);
                disp(cons, str);
                sprintf(str, "  frameSize      \t: %d", taskInfo->frameSize);
                disp(cons, str);
                sprintf(str, "  delay1         \t: %d", taskInfo->delay1);
                disp(cons, str);
                sprintf(str, "  delayM         \t: %d", taskInfo->delayM);
                disp(cons, str);
                sprintf(str, "  epqMin         \t: %.2f", taskInfo->epqMin);
                disp(cons, str);
                sprintf(str, "  svThresh       \t: %.2f", taskInfo->svThresh);
                disp(cons, str);
                sprintf(str, "  paramAoffset   \t: %d", taskInfo->paramAoffset);
                disp(cons, str);
                sprintf(str, "  doSpeechDetect \t: %d", taskInfo->doSpeechDetect);
                disp(cons, str);
                sprintf(str, "  checkPhraseQuality \t: %.2f", taskInfo->checkPhraseQuality);
                disp(cons, str);
                sprintf(str, "  checkEnergyMin     \t: %.2f", taskInfo->checkEnergyMin);
                disp(cons, str);
                sprintf(str, "  checkEnergyStdDev  \t: %.2f", taskInfo->checkEnergyStdDev);
                disp(cons, str);
                sprintf(str, "  checkSilBegMsec    \t: %.2f", taskInfo->checkSilBegMsec);
                disp(cons, str);
                sprintf(str, "  checkSilEndMsec    \t: %.2f", taskInfo->checkSilEndMsec);
                disp(cons, str);
                sprintf(str, "  checkSNR           \t: %.2f", taskInfo->checkSNR);
                disp(cons, str);
                sprintf(str, "  checkRecordingVariance: %.2f", taskInfo->checkRecordingVariance);
                disp(cons, str);
                sprintf(str, "  checkPoorRecordLimit  : %.2f", taskInfo->checkPoorRecordingsLimit);
                disp(cons, str);
                sprintf(str, "  checkClippingFrames\t: %.2f", taskInfo->checkClippingFrames);
                disp(cons, str);
                sprintf(str, "  checkVowelDur      \t: %.2f", taskInfo->checkVowelDur);
                disp(cons, str);
                sprintf(str, "  checkRepetition    \t: %.2f", taskInfo->checkRepetition);
                disp(cons, str);
                sprintf(str, "  checkSilInPhrase   \t: %.2f", taskInfo->checkSilInPhrase);
                disp(cons, str);
                sprintf(str, "  checkSpot          \t: %.2f", taskInfo->checkSpot);
                disp(cons, str);
                thfAdaptTaskInfoDestroy(ses, aObj, taskName, taskInfo);
                disp(cons, "");
            }
        }

        /*------------------------------------------------------------------------------*/
        void printSdetStatus(void *cons, unsigned short int len, short *audio, int status) {

            switch (status) {
                case RECOG_QUIET:
                    disp(cons, "quiet");
                    break;
                case RECOG_SOUND:
                    disp(cons, "sound");
                    std::cout << "size of short: " << sizeof (unsigned short int) << "\tlen: " << len << "\n";
                    break;
                case RECOG_IGNORE:
                    disp(cons, "ignored");
                    break;
                case RECOG_SILENCE:
                    disp(cons, "timeout: silence");
                    break;
                case RECOG_MAXREC:
                    disp(cons, "timeout: maxrecord");
                    break;
                case RECOG_DONE:
                    disp(cons, "end-of-speech");
                    break;
                default:
                    disp(cons, "other");
                    break;
            }
        }

        /*------------------------------------------------------------------------------*/
        int Recorder::exceptionThrow(std::string err_message) {
            try {
                killAudio();
            } catch (std::string e) {
                std::cout << "Failed to kill Audio, Maybe it's not initiated yet.";
            }
            std::cout << err_message << "\n";
            closeConsole(cons);
            thfSessionDestroy(ses);
            return 1;
        }

        int Recorder::recogEnroll()//int main(int argc, char **argv)
        {
            /* Initialize audio */
            disp(cons, "Initializing audio...");
            initAudio(NULL, cons);
            stopRecord(); /* initAudio() starts recording immediately, which we don't want right now */

            /* --------- get all enrollment recordings from each user ---------*/

            /* uIdx may be non-null was cache file successfully loaded */
            //  for (; uIdx < numUsers; uIdx++) {
            /* Record enrollment phrases for each user */
            //    for (eIdx = 0; eIdx < numEnroll; eIdx++) {
            disp(cons, "Recording...");

            /* Pipelined recording */
            if (startRecord() == RECOG_NODATA) res = exceptionThrow("startRecord() failed");

            done = 0;
            rres = NULL;

            unsigned long full_len = 0;

            while (!done) /* Feeds audio into recognizer until speech detector returns done */ {
                audioEventLoop();
                while ((aud = audioGetNextBuffer(&done)) && !done) {
                    if (!thfRecogPipe(ses, enrollRecog, aud->len, aud->audio, SDET_ONLY, &status))
                        res = exceptionThrow("recogPipe");

                    printSdetStatus(cons, aud->len, aud->audio, status);
                    //      if (status == RECOG_SOUND)
                    {
                        
                        //communication_msg->buffer.insert(communication_msg->buffer.end(), aud->audio, aud->audio +  (aud->len));        
                        //memcpy(&voic[full_len], &aud->audio[0], (aud->len)*2);
                        full_len += (aud->len);
                    }
                    switch (status) {
                        case RECOG_DONE:
                        {
                            done = 1;
                            stopRecord();
                            break;
                        }
                        case RECOG_SILENCE:
                        {
                            done = 1;
                            stopRecord();
                            break;
                        }
                        case RECOG_MAXREC:
                        {
                            done = 1;
                            stopRecord();
                            break;
                        }
                        default:
                        std::cout << "Default Status\n";
                    }
                }
                audioNukeBuffer(aud);
                aud = NULL;
                if (aud) audioNukeBuffer(aud);

            }
            //short* voice_buffer_cpointer = voice_buffer->data();
            const short *sdetSpeech;
            unsigned long sdetSpeechLen;
            float score = 0;
            thfRecogResult(ses, enrollRecog, &score, &rres, NULL,
                    NULL, NULL, NULL, &sdetSpeech, &sdetSpeechLen);
            
            std::cout << "THE LENGTH OF SDETSPEECH: " << sdetSpeechLen << "\n";
            voice_buffer->insert(voice_buffer->end(), sdetSpeech, sdetSpeech+sdetSpeechLen);
            printf("last chunk inside recorder %x\n", (*voice_buffer)[(*voice_buffer).size()-1]);

            thfWaveSaveToFile(NULL, "recorded_statement.wav", (short*) sdetSpeech, sdetSpeechLen, 16000);
            printf("first  100 bytes of audio before send to server in recorder module\n");
            for (int i=0 ; i<50; i++) printf("%04x \n", sdetSpeech[i]);
            /*
            FILE* fp = fopen("/home/pi/Sonic/test/recorded_vector.wav", "w+b");
            fwrite(sdetSpeech, sizeof(short), sdetSpeechLen, fp);
            fclose(fp);
            */
            thfRecogReset(ses, enrollRecog);


            try {
                killAudio();
            } catch (std::string e) {
                std::cout << "Failed to kill Audio, Maybe it's not initiated yet.";
            }
            
            return res;
        }

        Recorder::Recorder(std::vector<short>& voice_buffer_, HardwareMsg& hwmsg_) {
            voice_buffer = &voice_buffer_;
            hwmsg = &hwmsg_;
            //communication_msg = &communication_msg_;
            //voic = (short *) malloc(500000);
            //char *qualityDescription[] = {"VERY POOR", "POOR", "FAIR", "GOOD"};
            char taskType[] = "trigger";
            char taskName[] = "trigger";
            thfEnrollmentCheck_t *enrollmentCheckResult = NULL;

            char* netFileChar = new char[netFile.length() + 1];
            strcpy(netFileChar, netFile.c_str());

            char* taskFileChar = new char[taskFile.length() + 1];
            strcpy(taskFileChar, taskFile.c_str());

            /* Draw console */
            try {
                cons = initConsole(NULL);
                if (!cons) throw "Init Console Failed";
            } catch (std::string e) {
                std::cout << e + "\n";
            }
            /* Create SDK session */
            try {
                ses = thfSessionCreate();
                if (!ses) throw "Session Create Failed";
            } catch (std::string e) {
                panic(cons, "thfSessionCreate", thfGetLastError(NULL), 0);
                std::cout << e + "\n";
            }

            /* Initialize adapt task */
            if (!(aObj = thfAdaptCreate(ses)))
                res = exceptionThrow("thfAdaptCreate");
            if (!thfAdaptTaskCreate(ses, aObj, taskType, taskFileChar, taskName))
                res = exceptionThrow("thfAdaptTaskCreate");
            printTaskInfo(cons, NULL, NULL, ses, aObj, taskName);

            /* Optional:  Save the enrolled audio (useful for transferring as a package perhaps) */
            if (!thfAdaptTaskSet(ses, aObj, taskName, "ADAPT_SAVE_ENROLLMENT_AUDIO", 1.0))
                res = exceptionThrow("thfAdaptTaskSet");

            /* Recognizer - used for speech detection of enrollment phrases */

            if (!(enrollRecog = thfRecogCreateFromFile(ses, netFileChar, 0xFFFF, -1, SDET)))
                res = exceptionThrow("thfRecogCreateFromFile");
            if (!thfRecogConfigSet(ses, enrollRecog, REC_LSILENCE, lsil))
                res = exceptionThrow("thfRecogConfigSet: lsil");
            if (!thfRecogConfigSet(ses, enrollRecog, REC_TSILENCE, tsil))
                res = exceptionThrow("thfRecogConfigSet: tsil");
            if (!thfRecogConfigSet(ses, enrollRecog, REC_MAXREC, maxR))
                res = exceptionThrow("thfRecogConfigSet: maxrec");
            if (!thfRecogConfigSet(ses, enrollRecog, REC_MINDUR, minDur))
                res = exceptionThrow("thfRecogConfigSet: mindur");
            if (!thfRecogConfigSet(ses, enrollRecog, REC_SHORTTERMMS, shortterm))
                res = exceptionThrow("thfRecogConfigSet: shortterm");
            if (!thfRecogConfigSet(ses, enrollRecog, REC_LONGTERMMS, longterm))
                res = exceptionThrow("thfRecogConfigSet: longterm");
            //    if (!thfRecogConfigSet(ses, enrollRecog, REC_ESILENCET, energy)) 
            //        res = exceptionThrow("thfRecogConfigSet: energy");
            if (!thfRecogInit(ses, enrollRecog, NULL, RECOG_KEEP_WAVE))
                res = exceptionThrow("thfRecogInit");

        }

        Recorder::~Recorder() {

            if (enrollRecog) thfRecogDestroy(enrollRecog);
            if (cons) closeConsole(cons);
            if (ses) thfSessionDestroy(ses);

        }
    }// namespace AIP
}//namespace Sonic
