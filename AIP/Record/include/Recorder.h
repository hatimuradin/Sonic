/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Recorder.h
 * Author: Hatef Madani
 *
 * Created on June 14, 2018, 7:39 PM
 */

/*
 * Sensory Confidential
 * Copyright (C)2000-2015 Sensory Inc.
 *
 *---------------------------------------------------------------------------
 * Sensory TrulyHandsfree SDK Example: recogEnroll.c
 * This example shows demonstrates the enrollment and testing phases of 
 * enrolled fixed triggers (EFT) and user-defined triggers (UDT).
 *
 * NOTE: The same basic steps are followed to create both an enrolled fixed trigger
 * and user-defined trigger. The trigger type is determined by the specific 
 * task file used during initialization.
 *
 *---------------------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include "console.h"
#include "audio.h"
#include "trulyhandsfree.h"
#include "HardwareMsg.h"

//# include "datalocations.h"  /* Specifies which language pack to use */
using namespace Sonic::Common::Messages;
        
namespace Sonic{
    namespace AIP{
        class Recorder{
        private:
            std::string THNET_FILE = "en_us_16kHz_v10/nn_en_us_mfcc_16k_15_250_v5.1.1.raw";
            std::string netFile = "/home/pi/Sonic/AIP/Sensory/Data/" + THNET_FILE;
            //# define TASK_FILE    "UDT_enUS_taskData_v12_0_5.raw"  /* UDT Task file */
            std::string TASK_FILE = "UDT_enUS_taskData_v12_0_5.raw";
            std::string taskFile = "/home/pi/Sonic/AIP/Record/Data/" + TASK_FILE;
            /* Use the following macro to control whether to load data from the enrollment cache (if available). */
            /* The enrollment cache allows for modifying the enrollment set (e.g., adding additional users) at a later time */
            const int USE_CACHE = 1;/* Set to 0 to ignore cache or 1 to load cache (if exists) */
            //#define CACHE_FILE    "enrollCache.raw"     
            std::string CACHE_FILE = "enrollCache.raw";
            //#define ENROLLED_NETFILE     "enrollNet.raw"    /* Output: combined trigger acoustic model for all users */
            std::string ENROLLED_NETFILE = "enrollNet.raw";
            //#define ENROLLED_SEARCHFILE  "enrollSearch.raw" /* Output: combined trigger search for all users */
            std::string ENROLLED_SEARCHFILE = "enrollSearch.raw";
            //#define EMBEDDED_NETFILE     "enrollNet.bin"    /* Output: combined trigger recognizer for all users */
            std::string EMBEDDED_NETFILE = "enrollNet.bin";
            //#define EMBEDDED_SEARCHFILE  "enrollSearch.bin" /* Output: combined trigger search for all users */
            std::string EMBEDDED_SEARCHFILE = "enrollSearch.bin";
            
            const char* EMBEDDED_TARGET = "pc38";              /* Target used when saving embedded data files */

            const int NUM_USERS = 1; //2
            const int NUM_ENROLL = 1;//3
            const int NUM_TEST = 1;//5
            const int NBEST = 1;             /* Number of results  */
            const int MAXSTR = 512;/* Output string size */
            float SDET_LSILENCE = 6000;
            float SDET_TSILENCE = 750;//(500.f)
            float SDET_MAXRECORD = 60000;
            float SDET_MINDURATION = 500;
            float EPSILON = 1.0e-5;
            
            //short *voic;
            std::vector<short>* voice_buffer;
            
            int res = 0;
            int fail = 0;
            const char *ewhere = NULL, *ewhat = NULL;
            thf_t *ses = NULL;
            adaptObj_t *aObj = NULL;
            recog_t *enrollRecog = NULL; /* Used for speech detection during enrollment */
            recog_t *recogTrigger = NULL; /* Used for testing enrolled trigger */
            searchs_t *searchTrigger = NULL; /* used for testing enrolled trigger */
            const char *rres = NULL;
            char *str = new char[MAXSTR];
            unsigned short status, done = 0;
            audiol_t *aud = NULL;
            void *cons = NULL;
            float lsil = SDET_LSILENCE, tsil = SDET_TSILENCE, maxR = SDET_MAXRECORD, minDur = SDET_MINDURATION, shortterm = 300, longterm = 500, energy = 0.1;
            unsigned short numUsers = NUM_USERS;
            unsigned short numEnroll = NUM_ENROLL;
            unsigned short eIdx, uIdx = 0;
            thfAdaptUserInfo_t *userInfo = NULL;
            thfModelTrigger_t *modelTrigger = NULL;
            int qualityStrIdx = 0;
            const char *taskType;
            const char *taskName;
            float svThresh; // This phrasespot param gets returned by enrollment
            
            //hardware message (mute botton)
            HardwareMsg* hwmsg;

        public:
            Recorder(std::vector<short>&, HardwareMsg&);
            ~Recorder();
            void printValue(void*, void*, void*, int, const char*, thfEnrollmentCheckOne_t*, thfEnrollmentCheckOne_t*);
            void printValues(void*, void*, void*, thfEnrollmentCheck_t*, int);
            void printEnrollFeedback(void*, void*, void*, thfEnrollmentCheck_t*);
            void printTaskInfo(void*, void*, void*, thf_t*, adaptObj_t*, char*);
            int exceptionThrow(std::string);
            int recogEnroll();
        };
    }
}


