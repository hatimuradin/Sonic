#include <iostream>
#include <errno.h>
#include <wiringPiI2C.h>
#include <unistd.h>

using namespace std;

int main()
{
   int fd, result;
   char buf[4]={0};

///Setup License Chip
   fd = wiringPiI2CSetup(0x3C);
   cout << "Init result: "<< fd << "\n";

///Get Chip ID
   wiringPiI2CWrite (fd, 0x22) ;
   read(fd,buf,4);
   printf("Version ID : %x, %x, %x, %x\r\n",buf[0],buf[1],buf[2],buf[3]);


///Send Random 64bit Challenge Data
   char InData[9]={0x55,0x38,0x33,0x26,0x24,0x79,0x80,0x80,0x83};
   char OutData[8]={0};
   write(fd,InData,9);
 
///Response of License Chip
   wiringPiI2CWrite (fd, 0x66) ;
   read(fd,OutData,8);
   printf("Response : %x, %x, %x, %x,%x, %x, %x, %x\r\n",OutData[0],OutData[1],OutData[2],OutData[3],OutData[4],OutData[5],OutData[6],OutData[7]);

}