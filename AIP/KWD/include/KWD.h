/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*  * Author: Hatef Madani
 *
 * Created on May 15, 2018, 4:23 PM
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sstream>
#include <iostream>
#include <string>
#include "trulyhandsfree.h"
#include "console.h"
#include "audio.h"
#include "datalocations.h"
#include "HardwareMsg.h"

namespace Sonic {
    namespace AIP{

        using namespace Sonic::Common::Messages;

        class KWD {

        public:
            KWD(HardwareMsg&);
            ~KWD();
            int exceptionThrow(std::string);
            char* formatExpirationDate(time_t);
            int phraseSpot();
            
            private:
            const int MAXSTR = 512;
            const int MODE_LOAD_CUSTOM = 1; // Load pre-built custom HBG vocabulary from file (uses custom HBG acoustic model)
            const int MODE_BUILD_MANUAL = 2; // Build vocabulary with explicit phrasespot parameters (uses gerenic acoustic model)
            const int MODE_BUILD_AUTO = 3; // Build vocabulary with automatic phrasespot parameters (uses generic acoustic model)

            //#define BUILD_MODE        (MODE_LOAD_CUSTOM)

            const std::string HBG_NETFILE = "/home/pi/Sonic/AIP/KWD/Data/thfft_alexa_a_enus_v3_1mb_am.raw";
            const std::string HBG_SEARCHFILE = "/home/pi/Sonic/AIP/KWD/Data/thfft_alexa_a_enus_v3_1mb_search_9.raw";
            const std::string HBG_OUTFILE = "/home/pi/Sonic/AIP/KWD/Data/myhbg.raw"; /* hbg output search */

            //#define NBEST              (1)                /* Number of results 
            const int NBEST = 1;
            //#define PHRASESPOT_BEAM    (100.f)            /* Pruning beam */
            const float PHRASESPOT_BEAM = 100;
            //#define PHRASESPOT_ABSBEAM (100.f)            /* Pruning absolute beam */
            const float PHRASESPOT_ABSBEAM = 100;

            //# define PHRASESPOT_DELAY  10//PHRASESPOT_DELAY_ASAP  /* Phrasespotting Delay */
            const int PHRASESPOT_DELAY = 10;
            
            char* str = new char[MAXSTR];
            const char *rres;
            thf_t *ses = NULL;
            recog_t *r = NULL;
            searchs_t *s = NULL;
            pronuns_t *pp = NULL;
            const char *ewhere, *ewhat = NULL;
            float score, delay;
            unsigned short status, done = 0;
            audiol_t *p;
            unsigned short i = 0;
            void *cons = NULL;
            FILE *fp = NULL;
            int res = 0;    //normal return: 0, exception: 1, hardware mute: 2
            
            //hardware message (mute botton)
            HardwareMsg* hwmsg;
            
        };
    }// namespace AIP
}// namespace Sonic


