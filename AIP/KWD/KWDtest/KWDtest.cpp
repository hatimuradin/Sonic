/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Mahdi
 *
 * Created on May 15, 2018, 4:22 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <thread>
#include "KWD.h"
#include <iostream>
#include <string.h>
/*
 * 
 */
using namespace Sonic::Common::Messages;

void tester(HardwareMsg& hwmsg){
    usleep(10000000);
    hwmsg.mute_botton = true;
}
int main()
{
    HardwareMsg* hwmsg = new HardwareMsg();
    Sonic::AIP::KWD* detector = new Sonic::AIP::KWD(*hwmsg);
    std::thread t(tester, std::ref(*hwmsg));
    detector->phraseSpot();
    t.join();
    delete detector;
    return 0;
}

