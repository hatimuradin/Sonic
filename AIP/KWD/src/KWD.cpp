/*
 * Sensory Confidential
 *
 * Copyright (C)2000-2015 Sensory Inc.
 *
 *---------------------------------------------------------------------------
 * Sensory TrulyHandsfree SDK Example: PhraseSpot.c
 *---------------------------------------------------------------------------
 */
/* Specifies which language pack to use */

#include "KWD.h"
#include "Logger.h"

//#define MAXSTR             (512)     /* Output string size */

namespace Sonic {
    namespace AIP {

        using namespace std;
        
        static const std::string TAG("KWD");

        /**
         * Create a LogEntry using this file's TAG and the specified event string.
         *
         * @param The event string for this @c LogEntry.
         */
        #define LX(event) Sonic::Common::logger::LogEntry(TAG, event)

        //#define MODE_LOAD_CUSTOM  (1)  // Load pre-built custom HBG vocabulary from file (uses custom HBG acoustic model)
        //#define MODE_BUILD_MANUAL (2)  // Build vocabulary with explicit phrasespot parameters (uses gerenic acoustic model)
        //#define MODE_BUILD_AUTO   (3)  // Build vocabulary with automatic phrasespot parameters (uses generic acoustic model)

        //#define BUILD_MODE        (MODE_LOAD_CUSTOM)

        //#define HBG_NETFILE      "../Data/thfft_alexa_a_enus_v3_1mb_am.raw"
        //#define HBG_SEARCHFILE   "../Data/thfft_alexa_a_enus_v3_1mb_search_9.raw"
        //#define HBG_OUTFILE      "../Data/myhbg.raw"           /* hbg output search */

        //#define NBEST              (1)                /* Number of results 

        //#define PHRASESPOT_BEAM    (100.f)            /* Pruning beam */
        //#define PHRASESPOT_ABSBEAM (100.f)            /* Pruning absolute beam */

        //# define PHRASESPOT_DELAY  10//PHRASESPOT_DELAY_ASAP  /* Phrasespotting Delay */

        //#define inst NULL
        //#define job NULL

        int KWD::exceptionThrow(std::string message) {
            try {
                killAudio();
            } catch (std::string e) {
                cout << "Unable to kill Audio, Maybe it's not initiated.\n";
            }
            //if (!ewhat && ses) ewhat = thfGetLastError(ses);
            if (fp) fclose(fp);
            if (r) thfRecogDestroy(r);
            if (s) thfSearchDestroy(s);
            if (pp) thfPronunDestroy(pp);
            //panic(cons, ewhere, ewhat, 0);
            if (ses) thfSessionDestroy(ses);
        }

        char* KWD::formatExpirationDate(time_t expiration) {
            static char expdate[33];
            if (!expiration) return "never";
            strftime(expdate, 32, "%m/%d/%Y 00:00:00 GMT", gmtime(&expiration));
            return expdate;
        }

        int KWD::phraseSpot() {

            /* Initialize audio */
            ACSDK_INFO(LX("Starting Audio"));
            initAudio(NULL, cons);

            disp(cons, "Recording...");
            if (startRecord() != RECOG_NODATA) {
                disp(cons, "\r>>> Say 'Alexa' <<<");

                /* Pipelined recognition */
                done = 0;
                while (!done && !hwmsg->mute_botton) {
                    audioEventLoop();
                    while ((p = audioGetNextBuffer(&done)) && !done) {
                        if (!thfRecogPipe(ses, r, p->len, p->audio, RECOG_ONLY, &status))
                            exceptionThrow("recogPipe");
                        audioNukeBuffer(p);

                        switch (status) {
                            case RECOG_DONE:
                            {
                                done = 1;
                                stopRecord();
                                break;
                            }
                            case RECOG_SILENCE:
                            {
                                done = 1;
                                stopRecord();
                                break;
                            }
                            case RECOG_MAXREC:
                            {
                                done = 1;
                                stopRecord();
                                break;
                            }
                            default:
                                std::cout << "Still recording\n\r";
                        }
                    }
                }
                //Set return value accourding to loop exit state
                (hwmsg->mute_botton) ? (res = 2) : (res = 0);

                /* Report N-best recognition result */
                rres = NULL;
                if (status == RECOG_DONE) {
                    disp(cons, "Recognition results...");
                    score = 0;
                    if (!thfRecogResult(ses, r, &score, &rres, NULL, NULL, NULL, NULL, NULL, NULL))
                        exceptionThrow("thfRecogResult");
                    sprintf(str, "Result: %s (%.3f)", rres, score);
                    disp(cons, str); //to do here. call another app and go.

                    //***************************************//
                    disp(cons, "Alexa detected. Now sey your command. I'm listening...\n");
                    // sprintf(str,"RecogEnroll result is: %i",recogEnroll()); disp(cons,str);
                    //***************************************//
                } else
                    disp(cons, "No recognition result");
                thfRecogReset(ses, r);
            }
            /* Async error handling, see console.h for panic */
            std::cout << "killing audio at end of phraseSpot\n";
            killAudio();

            return res;
        }

        KWD::KWD(HardwareMsg& hwmsg_) {
            hwmsg = &hwmsg_;
            /* Draw console */
            try {
                cons = initConsole(NULL);
                if (!cons) throw "Init Console Failed";
            } catch (std::string error_message) {
                exceptionThrow(error_message);
            }
            /* Create SDK session */
            try {
                ses = thfSessionCreate();
                if (!ses) throw "Session Create Failed";
            } catch (std::string error_message) {
                panic(cons, "thfSessionCreate", thfGetLastError(NULL), 0);
                exceptionThrow(error_message);
            }

            /* Display TrulyHandsfree SDK library version number */
            dispv(cons, "TrulyHandsfree SDK Library version: %s\n", thfVersion());
            dispv(cons, "Expiration date: %s\n\n",
                    formatExpirationDate(thfGetLicenseExpiration()));

            /* Create recognizer */

            /* NOTE: HBG_NETFILE is a custom model for 'hello blue genie'. Use a generic acoustic model if you change vocabulary or contact Sensory for assistance in developing a custom acoustic model specific to your vocabulary. */
            disp(cons, ("Loading recognizer: " + HBG_NETFILE).c_str());
            if (!(r = thfRecogCreateFromFile(ses, HBG_NETFILE.c_str(), (unsigned short) (AUDIO_BUFFERSZ / 1000.f * SAMPLERATE), -1, NO_SDET)))
                exceptionThrow("thfRecogCreateFromFile");

            disp(cons, ("Loading custom HBG search: " + HBG_SEARCHFILE).c_str());
            if (!(s = thfSearchCreateFromFile(ses, r, HBG_SEARCHFILE.c_str(), NBEST)))
                exceptionThrow("thfSearchCreateFromFile");

            /* Configure parameters - only DELAY... others are saved in search already */
            disp(cons, "Setting parameters...");
            delay = PHRASESPOT_DELAY;
            if (!thfPhrasespotConfigSet(ses, r, s, PS_DELAY, delay))
                exceptionThrow("thfPhrasespotConfigSet: delay");


            if (thfRecogGetSampleRate(ses, r) != SAMPLERATE)
                exceptionThrow("Acoustic model is incompatible with audio samplerate");


            /* Initialize recognizer */
            disp(cons, "Initializing recognizer...");

            if (!thfRecogInit(ses, r, s, RECOG_KEEP_WAVE_WORD_PHONEME))
                exceptionThrow("thfRecogInit");

            /* initialize a speaker object with a single speaker,
             * and arbitrarily set it up
             * to hold one recording from this speaker
             */
            if (!thfSpeakerInit(ses, r, 0, 1)) {
                exceptionThrow("thfSpeakerInit");
            }

        }

        KWD::~KWD() {
            thfRecogDestroy(r);
            r = NULL;
            thfSearchDestroy(s);
            s = NULL;
            thfPronunDestroy(pp);
            pp = NULL;
            thfSessionDestroy(ses);
            ses = NULL;
            disp(cons, "Done");
            closeConsole(cons);
        }
    }; // AIP namespace
}; // Sonic namespace