

#include <cstdio>
#include <ctime>
#include <iostream>
#include <mutex>

#include "ConsoleLogger.h"
#include "LoggerUtils.h"
#include "ThreadMoniker.h"

namespace Sonic {
namespace Common {
namespace logger {

/// Configuration key for DefaultLogger settings
static const std::string CONFIG_KEY_DEFAULT_LOGGER = "consoleLogger";

std::shared_ptr<Logger> ConsoleLogger::instance() {
    static std::shared_ptr<Logger> singleConsoletLogger = std::shared_ptr<ConsoleLogger>(new ConsoleLogger);
    return singleConsoletLogger;
}

void ConsoleLogger::emit(
    Level level,
    std::chrono::system_clock::time_point time,
    const char* threadMoniker,
    const char* text) {
    std::lock_guard<std::mutex> lock(m_coutMutex);
    std::cout << m_logFormatter.format(level, time, threadMoniker, text) << std::endl;
}

ConsoleLogger::ConsoleLogger() : Logger(Level::UNKNOWN) {
#ifdef DEBUG
    setLevel(Level::DEBUG0);
#else
    setLevel(Level::INFO);
#endif  // DEBUG
    init(ConfigurationNode::getRoot()[CONFIG_KEY_DEFAULT_LOGGER]);
    std::string currentVersionLogEntry("sdkVersion: 1.0.0");
    emit(
        Sonic::Common::logger::Level::INFO,
        std::chrono::system_clock::now(),
        ThreadMoniker::getThisThreadMoniker().c_str(),
        currentVersionLogEntry.c_str());
}

std::shared_ptr<Logger> getConsoleLogger() {
    return ConsoleLogger::instance();
}

}  // namespace logger
}  // namespace Common
}  // namespace Sonic
