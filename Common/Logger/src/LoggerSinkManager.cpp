

#include <algorithm>

#include "LoggerSinkManager.h"

namespace Sonic {
namespace Common {
namespace logger {

LoggerSinkManager& LoggerSinkManager::instance() {
    static LoggerSinkManager singleLoggerSinkManager;
    return singleLoggerSinkManager;
}

void LoggerSinkManager::addSinkObserver(SinkObserverInterface* observer) {
    if (!observer) {
        return;
    }

    {
        std::lock_guard<std::mutex> lock(m_sinkObserverMutex);
        m_sinkObservers.push_back(observer);
    }

    // notify this observer of the current sink right away
    observer->onSinkChanged(m_sink);
}

void LoggerSinkManager::removeSinkObserver(SinkObserverInterface* observer) {
    if (!observer) {
        return;
    }
    std::lock_guard<std::mutex> lock(m_sinkObserverMutex);
    m_sinkObservers.erase(std::remove(m_sinkObservers.begin(), m_sinkObservers.end(), observer), m_sinkObservers.end());
}

void LoggerSinkManager::initialize(const std::shared_ptr<Logger>& sink) {
    if (m_sink == sink) {
        // don't do anything if the sink is the same
        return;
    }

    // copy the vector first with the lock
    std::vector<SinkObserverInterface*> observersCopy;
    {
        std::lock_guard<std::mutex> lock(m_sinkObserverMutex);
        observersCopy = m_sinkObservers;
    }

    m_sink = sink;

    // call the callbacks
    for (auto observer : observersCopy) {
        observer->onSinkChanged(m_sink);
    }
}

LoggerSinkManager::LoggerSinkManager() : m_sink(ACSDK_GET_SINK_LOGGER()) {
}

}  // namespace logger
}  // namespace Common
}  // namespace Sonic
