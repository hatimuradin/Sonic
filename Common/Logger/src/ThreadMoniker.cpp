

#include <atomic>
#include <iomanip>
#include <sstream>
#include <thread>
#include "ThreadMoniker.h"

namespace Sonic {
namespace Common {
namespace logger {

/// Counter to generate (small) unique thread monikers.
static std::atomic<int> g_nextThreadMoniker(1);

ThreadMoniker::ThreadMoniker() {
    std::ostringstream stream;
    stream << std::setw(3) << std::hex << std::right << g_nextThreadMoniker++;
    m_moniker = stream.str();
}

}  // namespace logger
}  // namespace Common
}  // namespace Sonic
