

#include "LogEntryStream.h"

namespace Sonic {
namespace Common {
namespace logger {

LogEntryStream::LogEntryStream() : LogEntryBuffer{}, std::ostream{this} {
}

const char* LogEntryStream::LogEntryStream::c_str() const {
    return LogEntryBuffer::c_str();
}

}  // namespace logger
}  // namespace Common
}  // namespace Sonic
