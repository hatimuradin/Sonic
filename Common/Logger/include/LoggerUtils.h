

#ifndef SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_LOGGERUTILS_H_
#define SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_LOGGERUTILS_H_

#include <chrono>

#include "Logger.h"

namespace Sonic {
namespace Common {
namespace logger {

/**
 * Log a LEVEL::DEBUG9 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug9(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG8 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug8(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG7 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug7(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG6 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug6(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG5 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug5(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG4 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug4(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG3 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug3(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG2 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug2(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG1 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug1(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG0 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug0(const LogEntry& entry);

/**
 * Log a LEVEL::DEBUG0 severity entry.
 *
 * @param entry A log entry.
 */
void acsdkDebug(const LogEntry& entry);

/**
 * Log a Level::INFO severity entry.
 *
 * @param entry A log entry.
 */
void acsdkInfo(const LogEntry& entry);

/**
 * Log a Level::WARN severity entry.
 *
 * @param entry A log entry.
 */
void acsdkWarn(const LogEntry& entry);

/**
 * Log a Level::ERROR severity entry.
 *
 * @param entry A log entry.
 */
void acsdkError(const LogEntry& entry);

/**
 * Log a Level::CRITICAL severity entry.
 *
 * @param entry A log entry.
 */
void acsdkCritical(const LogEntry& entry);

/**
 * Logs an entry at a severity level using the logger instance defined by
 * ACSDK_GET_LOGGER_FUNCTION (refer to Logger.h).
 *
 * @param level The severity level of the log.
 * @param entry A log entry.
 */
void logEntry(Level level, const LogEntry& entry);

/**
 * Stream out an array of bytes as a hex dump.
 *
 * @param stream The stream to render to.
 * @param prefix A prefix added to each row.
 * @param width The number of bytes to output per row.
 * @param data The bytes to render.
 * @param size The number of bytes to render.
 */
void dumpBytesToStream(std::ostream& stream, const char* prefix, size_t width, const unsigned char* data, size_t size);

}  // namespace logger
}  // namespace Common
}  // namespace Sonic

#endif  // SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_LOGGERUTILS_H_
