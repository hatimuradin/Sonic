

#ifndef SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_CONSOLELOGGER_H_
#define SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_CONSOLELOGGER_H_

#include "Logger.h"
#include "LoggerUtils.h"
#include "LogStringFormatter.h"

namespace Sonic {
namespace Common {
namespace logger {

/**
 * A very simple (e.g. not asynchronous) @c Logger that logs to console.
 */
class ConsoleLogger : public Logger {
public:
    /**
     * Return the one and only @c ConsoleLogger instance.
     *
     * @return The one and only @c ConsoleLogger instance.
     */
    static std::shared_ptr<Logger> instance();

    void emit(Level level, std::chrono::system_clock::time_point time, const char* threadMoniker, const char* text)
        override;

private:
    /**
     * Constructor.
     */
    ConsoleLogger();

    std::mutex m_coutMutex;

    /// Object to format log strings correctly.
    LogStringFormatter m_logFormatter;
};

/**
 * Return the singleton instance of @c ConsoleLogger.
 *
 * @return The singleton instance of @c ConsoleLogger.
 */
std::shared_ptr<Logger> getConsoleLogger();

}  // namespace logger
}  // namespace Common
}  // namespace Sonic

#endif  // SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_CONSOLELOGGER_H_
