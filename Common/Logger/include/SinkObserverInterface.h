

#ifndef SONIC_CLIENT_SDK_INCLUDE_LOGGER_SINKOBSERVERINTERFACE_H_
#define SONIC_CLIENT_SDK_INCLUDE_LOGGER_SINKOBSERVERINTERFACE_H_

#include <memory>

namespace Sonic {
namespace Common {
namespace logger {

// forward declaration
class Logger;

/**
 * This interface class allows notifications when the sink Logger changes.
 */
class SinkObserverInterface {
public:
    /**
     * This function will be called when the sink changes.
     *
     * @param sink The updated sink @c Logger
     */
    virtual void onSinkChanged(const std::shared_ptr<Logger>& sink) = 0;
};

}  // namespace logger
}  // namespace avsCommon
}  // namespace alexaClientSDK

#endif  // SONIC_CLIENT_SDK_INCLUDE_LOGGER_SINKOBSERVERINTERFACE_H_
