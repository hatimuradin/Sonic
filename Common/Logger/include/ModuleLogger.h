

#ifndef SONIC_CLIENT_SDK_INCLUDE_LOGGER_MODULELOGGER_H_
#define SONIC_CLIENT_SDK_INCLUDE_LOGGER_MODULELOGGER_H_

#include "Logger.h"

namespace Sonic {
namespace Common {
namespace logger {

/**
 * @c Logger implementation providing per module configuration. Forwards logs to another @c Logger.
 */
class ModuleLogger
        : public Logger
        , protected LogLevelObserverInterface
        , protected SinkObserverInterface {
public:
    /**
     * Constructor.
     *
     * @param configKey The name of the root configuration key to inspect for a "logLevel" string value. That
     * string is used to specify the lowest log severity level that this @c ModuleLogger should emit.
     */
    ModuleLogger(const std::string& configKey);

    void setLevel(Level level) override;

    void emit(Level level, std::chrono::system_clock::time_point time, const char* threadId, const char* text) override;

private:
    void onLogLevelChanged(Level level) override;

    void onSinkChanged(const std::shared_ptr<Logger>& sink) override;

    /**
     * Combine @c m_moduleLogLevel and @c m_sinkLogLevel to determine the appropriate value for m_logLevel.
     */
    void updateLogLevel();

    /// Log level specified for this module logger.
    Level m_moduleLogLevel;

    /// Log level specified for the sink to forward logs to.
    Level m_sinkLogLevel;

protected:
    /// The @c Logger to forward logs to.
    std::shared_ptr<Logger> m_sink;
};

}  // namespace logger
}  // namespace Common
}  // namespace Sonic

#endif  // SONIC_CLIENT_SDK_INCLUDE_LOGGER_MODULELOGGER_H_
