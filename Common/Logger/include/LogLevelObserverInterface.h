

#ifndef SONIC_CLIENT_SDK_INCLUDE_LOGGER_LOGLEVELOBSERVERINTERFACE_H_
#define SONIC_CLIENT_SDK_INCLUDE_LOGGER_LOGLEVELOBSERVERINTERFACE_H_

#include "Level.h"

namespace Sonic {
namespace Common {
namespace logger {

/**
 * This interface class allows notifications from a Logger object (or any derived class) when the
 * logLevel changes.
 */
class LogLevelObserverInterface {
public:
    /**
     * This function will be called when the logLevel changes.
     *
     * @param status The updated logLevel
     */
    virtual void onLogLevelChanged(Level level) = 0;
};

}  // namespace logger
}  // namespace Common
}  // namespace Sonic

#endif  // SONIC_CLIENT_SDK_INCLUDE_LOGGER_LOGLEVELOBSERVERINTERFACE_H_
