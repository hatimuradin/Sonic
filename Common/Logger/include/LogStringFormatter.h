

#ifndef SONIC_CLIENT_SDK_INCLUDE_LOGGER_LOGSTRINGFORMATTER_H_
#define SONIC_CLIENT_SDK_INCLUDE_LOGGER_LOGSTRINGFORMATTER_H_

#include <chrono>

#include "Logger.h"
#include "SafeCTimeAccess.h"

namespace Sonic {
namespace Common {
namespace logger {

/**
 * A class used to format log strings.
 */
class LogStringFormatter {
public:
    LogStringFormatter();

    /**
     * Formats a log message into a printable string with other metadata regarding the log message.
     *
     * @param level The severity Level of this log line.
     * @param time The time that the event to log occurred.
     * @param threadMoniker Moniker of the thread that generated the event.
     * @param text The text of the entry to log.
     * @return The formatted string.
     */
    std::string format(
        Level level,
        std::chrono::system_clock::time_point time,
        const char* threadMoniker,
        const char* text);

private:
    std::shared_ptr<SafeCTimeAccess> m_safeCTimeAccess;
};

}  // namespace logger
}  // namespace Common
}  // namespace Sonic

#endif  // SONIC_CLIENT_SDK_INCLUDE_LOGGER_LOGSTRINGFORMATTER_H_
