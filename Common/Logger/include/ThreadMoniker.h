

#ifndef SONIC_CLIENT_SDK_INCLUDE_LOGGER_THREADMONIKER_H_
#define SONIC_CLIENT_SDK_INCLUDE_LOGGER_THREADMONIKER_H_

#include <iomanip>
#include <string>
#include <sstream>
#include <thread>

namespace Sonic {
namespace Common {
namespace logger {

/**
 * Class to provide @c std::this_thread access to unique name for itself.
 * The name ThreadMoniker is used instead of ThreadId to avoid confusion with platform specific thread identifiers
 * or the @c std::thread::id values rendered as a string.
 */
class ThreadMoniker {
public:
    /**
     * Constructor.
     */
    ThreadMoniker();

    /**
     * Get the moniker for @c std::this_thread.
     *
     * @return The moniker for @c std::this_thread.
     */
    static inline const std::string getThisThreadMoniker();

private:
    /// The current thread's moniker.
    std::string m_moniker;
};

const std::string ThreadMoniker::getThisThreadMoniker() {
#ifdef _WIN32
    std::ostringstream winThreadID;
    winThreadID << std::setw(3) << std::hex << std::right << std::this_thread::get_id();
    return winThreadID.str();
#else
    /// Per-thread static instance so that @c m_threadMoniker.m_moniker is @c std::this_thread's moniker.
    static thread_local ThreadMoniker m_threadMoniker;

    return m_threadMoniker.m_moniker;
#endif
}

}  // namespace logger
}  // namespace Common
}  // namespace Sonic

#endif  // SONIC_CLIENT_SDK_INCLUDE_LOGGER_THREADMONIKER_H_
