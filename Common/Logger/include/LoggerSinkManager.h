

#ifndef SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_LOGGERSINKMANAGER_H_
#define SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_LOGGERSINKMANAGER_H_

#include <atomic>
#include <mutex>
#include <vector>

#include "Logger.h"

namespace Sonic {
namespace Common {
namespace logger {

/**
 * A manager to manage the sink logger and notify SinkObservers of any changes.
 */
class LoggerSinkManager {
public:
    /**
     * Return the one and only @c LoggerSinkManager instance.
     *
     * @return The one and only @c LoggerSinkManager instance.
     */
    static LoggerSinkManager& instance();

    /**
     * Add a SinkObserver to the manager.
     *
     * @param observer The @c SinkObserverInterface be be added.
     */
    void addSinkObserver(SinkObserverInterface* observer);

    /**
     * Remove a SinkObserver from the manager.
     *
     * @param observer The @c SinkObserverInterface to be removed.
     */
    void removeSinkObserver(SinkObserverInterface* observer);

    /**
     * Initialize the sink logger managed by the manager.
     * This function can be called only before any other threads in the process have been created by the
     * program.
     *
     * @param sink The new @c Logger to forward logs to.
     *
     * @note If this function is not called, the default sink logger
     * will be the one returned by get<ACSDK_LOG_SINK>Logger().
     */
    void initialize(const std::shared_ptr<Logger>& sink);

private:
    /**
     * Constructor.
     */
    LoggerSinkManager();

    /// This mutex guards access to m_sinkObservers.
    std::mutex m_sinkObserverMutex;

    /// Vector of SinkObservers to be managed.
    std::vector<SinkObserverInterface*> m_sinkObservers;

    /// The @c Logger to forward logs to.
    std::shared_ptr<Logger> m_sink;
};

}  // namespace logger
}  // namespace Common
}  // namespace Sonic

#endif  // SONIC_CLIENT_SDK_UTILS_INCLUDE_LOGGER_LOGGERSINKMANAGER_H_
