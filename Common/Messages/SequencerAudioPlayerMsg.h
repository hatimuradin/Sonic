/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommunicationSequencerMsg.h
 * Author: Hatef
 *
 * Created on June 23, 2018, 12:37 PM
 */
#include <queue>
#include <vector>

#ifndef SEQUENCER_AUDIO_PLYAER_H
#define SEQUENCER_AUDIO_PLAYER_H

namespace Sonic{
    namespace Common{
        namespace Messages{

            typedef struct AudioPlayerMessageT{
                std::string method;
                std::string uri;
                std::vector<char> audio;
            } AudioPlayerMessage;

            class SequencerAudioPlayerMsg{
            public:   
                std::queue<AudioPlayerMessage> messages_queue;
                SequencerAudioPlayerMsg(){};
                ~SequencerAudioPlayerMsg(){};
            private:
            };
        }
    }
}

#endif /* SEQUENCER_AUDIOPLAYER_H */

