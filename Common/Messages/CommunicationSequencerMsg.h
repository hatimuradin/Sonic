/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommunicationSequencerMsg.h
 * Author: Hatef
 *
 * Created on June 23, 2018, 12:37 PM
 */
#include <queue>
#include <vector>

#ifndef COMMUNICATION_SEQUENCER_H
#define COMMUNICATION_SEQUENCER_H

namespace Sonic{
    namespace Common{
        namespace Messages{

            typedef struct SequencerMessageT{
                std::string json;
                std::vector<char> audio;
            } SequencerMessage;

            class CommunicationSequencerMsg{
            public:
                std::queue<SequencerMessage> messages_queue;
                CommunicationSequencerMsg(){};
                ~CommunicationSequencerMsg(){};
            private:
            };
        }
    }
}

#endif /* COMMUNICATION_SEQUENCER_H */

