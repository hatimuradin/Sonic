/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RecorderCommunicationMsg.h
 * Author: Mahdi
 *
 * Created on June 23, 2018, 4:57 PM
 */
#include <vector>
#include <mutex>

#ifndef RECORDERCOMMUNICATIONMSG_H
#define RECORDERCOMMUNICATIONMSG_H

namespace Sonic {
    namespace Common {
        namespace Messages {

            class RecorderCommunicationMsg {
            public:
                std::mutex mtx;
                int msg_full;
                int msg_id;
                int agent_id;
                unsigned int audio_length;
                
                std::vector<short> audio;
                
                template <typename Writer>
                void Serialize(Writer& writer) const {
                    writer.StartObject();
                    writer.Key("msg_id");
                    writer.Uint(msg_id);
                    writer.Key("agent_id");
                    writer.Uint(agent_id);
                    writer.EndObject();
                }
            private:

            };
        }
    }
}

#endif /* RECORDERCOMMUNICATIONMSG_H */

