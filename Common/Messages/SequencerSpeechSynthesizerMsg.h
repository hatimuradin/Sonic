/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommunicationSequencerMsg.h
 * Author: Hatef
 *
 * Created on June 23, 2018, 12:37 PM
 */
#include <queue>
#include <vector>

#ifndef SequencerSpeechSynthesizerMsg_H
#define SequencerSpeechSynthesizerMsg_H

namespace Sonic{
    namespace Common{
        namespace Messages{

            typedef struct SpeechSynthesizerMessageT{
                int option1;
                int option2;
                std::vector<char> audio;
            } SpeechSynthesizerMessage;

            class SequencerSpeechSynthesizerMsg{
            public:   
                std::queue<SpeechSynthesizerMessage> messages_queue;
                SequencerSpeechSynthesizerMsg(){};
                ~SequencerSpeechSynthesizerMsg(){};
            private:
            };
        }
    }
}

#endif /* SEQUENCER_SPEECHSYNTHESIZER_H */

